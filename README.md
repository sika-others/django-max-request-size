django-max-request-size
=======================

Return 403 if request body will be larger than MAX_REQUEST_SIZE

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, ondrej@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/django-max-request-size>
* GitHub: <https://github.com/sikaondrej/django-max-request-size>

## Installation

install via pip

    pip install django-max-request-size
    

## Usage

add to your `settings.py`

    MAX_REQUEST_SIZE = 10 * 1000 * 1000  # 10MB
    MIDDLEWARE_CLASSES += ("max_request_size.MaxRequestSizeMiddleware", )
