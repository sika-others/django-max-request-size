#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-max-request-size",
    version = "1.0.0",
    url = 'https://github.com/sikaondrej/django-max-request-size/',
    license = 'MIT',
    author = "Ondrej Sika",
    aurhor_email = "ondrej@ondrejsika.com",
    description = "Return 403 if request body will be larger than MAX_REQUEST_SIZE",
    py_modules = ["max_request_size", ],
    install_requires = [],
    include_package_data = True,
    zip_safe = False,
)
