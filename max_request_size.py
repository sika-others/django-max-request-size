from django.conf import settings
from django.http import HttpResponseForbidden


class MaxRequestSizeMiddleware:
    def process_request(self, request):
        if len(request.body) > getattr(settings, "MAX_REQUEST_SIZE", 1000000):
            return HttpResponseForbidden()